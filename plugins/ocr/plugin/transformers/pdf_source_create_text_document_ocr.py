import logging

import easyocr

from ingestum import transformers
from ingestum import sources
from ingestum import documents

__logger__ = logging.getLogger("ingestum")


class EasyOCREngine(transformers.pdf_source_create_text_document_ocr.BaseEngine):
    class Config:
        extra = "allow"

    type: str = "easyocr"

    def __init__(self):
        super().__init__()
        self._reader = easyocr.Reader(["en"])

    def read(self, img, rect, pdf_width, pdf_height):
        """Returns a list of element dicts extracted from the given rectangle
        using EasyOCR."""
        x, y, width, height = rect
        crop = img[y : y + height, x : x + width]

        img_width, img_height = img.shape[1], img.shape[0]
        pdf_width_scaler = pdf_width / float(img_width)
        pdf_height_scaler = pdf_height / float(img_height)

        # Result is a list with the bounding box (four coords, clockwise from
        # top left), text, and confidence level.
        text_items = self._reader.readtext(crop)
        elements = [
            {
                "left": int(min(e[0][0][0], e[0][3][0]) * pdf_width_scaler),
                "right": int(max(e[0][1][0], e[0][2][0]) * pdf_width_scaler),
                "top": int(min(e[0][0][1], e[0][1][1]) * pdf_height_scaler),
                "bottom": int(max(e[0][2][1], e[0][3][1]) * pdf_height_scaler),
                "text": e[1],
                "page_width": pdf_width,
            }
            for e in text_items
        ]

        return elements


class Transformer(transformers.pdf_source_create_text_document_ocr.Transformer):
    def transform(self, source: sources.PDF) -> documents.Text:
        return super().transform(source=source)
